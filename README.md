# Sentiment Analysis [portuguese]

Basic server providing endpoint for sentiment analysis model inference.

# Setting up

### Resolve dependencies

```bash
pip install pipenv

pipenv install
```

### Model deployment

Make sure that:
 - `neuralmind/bert-base-portuguese-cased` model is in `models/bert_base/` folder
 - sentiment analysis model is in `models/sentiment_analysis/` folder

# Running the server

```bash
pipenv run uvicorn core.api:app
```

By default it will run at http://127.0.0.1:8000

# Running with Docker

```bash
sudo docker build . -t saportu

sudo docker run -p 8000:8000 saportu

curl -X POST http://127.0.0.1:8000/sa/predict -H 'Content-Type: application/json' -d '{"texts": ["você é uma pessoa muito solitária idiota!"]}'
```

# Endpoints

## /sa/predict

### Payload example

```json
{
    "texts": [
        "você é uma pessoa muito solitária idiota!",
        "que cara simpático você é!",
        "Eu não tenho uma opinião especial sobre este assunto"
    ]
}
```

### Response example

```json
[
    {
        "text": "você é uma pessoa muito solitária idiota!",
        "probabilities": {
            "negative": "0.78",
            "positive": "0.22"
        },
        "sentiment": "negative"
    },
    {
        "text": "que cara simpático você é!",
        "probabilities": {
            "negative": "0.01",
            "positive": "0.99"
        },
        "sentiment": "positive"
    },
    {
        "text": "Eu não tenho uma opinião especial sobre este assunto",
        "probabilities": {
            "negative": "0.38",
            "positive": "0.62"
        },
        "sentiment": "neutral"
    }
]
```
