import os
import torch
from torch.nn.functional import softmax
from transformers import BertForSequenceClassification, BertTokenizer

from core.conf import PROJECT_PATH


BERT_BASE_PATH = os.path.join(PROJECT_PATH, "models", "bert_base")
SA_MODEL_PATH = os.path.join(PROJECT_PATH, "models", "sentiment_analysis")


class Model:
    def __init__(self):
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.tokenizer = BertTokenizer.from_pretrained(BERT_BASE_PATH)
        self.model = BertForSequenceClassification.from_pretrained(SA_MODEL_PATH)
        self.model = self.model.eval()
        self.model = self.model.to(self.device)

    def predict(self, texts):
        encoded = self.tokenizer(texts, padding=True, return_tensors="pt")
        probs, preds = [], []
        with torch.no_grad():
            self.model.eval()
            for input_id, token_type_id, attention_mask in zip(encoded['input_ids'], encoded['token_type_ids'],
                                                               encoded['attention_mask']):
                input_id, token_type_id, attention_mask = input_id.to(self.device), token_type_id.to(
                    self.device), attention_mask.to(self.device)
                input_id, token_type_id, attention_mask = input_id.unsqueeze(dim=0), token_type_id.unsqueeze(
                    dim=0), attention_mask.unsqueeze(dim=0)
                output = self.model(input_ids=input_id, token_type_ids=token_type_id, attention_mask=attention_mask)
                proba = softmax(output.logits).detach().cpu().numpy().flatten()
                pred = proba.argmax()
                # print(f"predicted {pred}, probs {proba}, entropy {uncertainty}")
                probs.append(proba)
                preds.append(pred)

        verbose = []
        for text, proba, pred in zip(texts, probs, preds):
            if proba[pred] < 0.66:
                sentiment = 'neutral'
            else:
                sentiment = 'positive' if pred == 1 else 'negative'

            verbose.append({
                'text': text,
                'probabilities': {
                    'negative': str(round(float(proba[0]), 2)),
                    'positive': str(round(float(proba[1]), 2))
                },
                'sentiment': sentiment
            })

        return verbose


model = Model()


def get_model():
    return model
