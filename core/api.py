from typing import List
from fastapi import Depends, FastAPI
from pydantic import BaseModel

from core.sentiment import Model, get_model

app = FastAPI()


class SentimentRequest(BaseModel):
    texts: List[str]


@app.post("/sa/predict")
def predict(request: SentimentRequest, model: Model = Depends(get_model)):
    response = model.predict(request.texts)
    return response
