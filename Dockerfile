FROM ubuntu:18.04

RUN apt-get update && apt-get install -y software-properties-common gcc && \
    add-apt-repository -y ppa:deadsnakes/ppa

RUN apt-get update && apt-get install -y python3.6 python3-distutils python3-pip python3-apt

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY . .

RUN pip3 install pipenv && pipenv install

CMD [ "pipenv", "run" , "uvicorn", "core.api:app", "--host", "0.0.0.0", "--port", "8000"]
